/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.teamx.parking;

import com.teamx.parking.slice.MainAbilitySlice;
import com.teamx.parking.slice.OtherWifiSlice;
import com.teamx.parking.slice.QRCodeSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class MainAbility extends Ability {
    private final String[] permission = {"ohos.permission.LOCATION"};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        askPermission();
        addActionRoute("action.otherWifi", OtherWifiSlice.class.getName());
        addActionRoute("action.qrCode", QRCodeSlice.class.getName());
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.BLUE.getValue()); // 设置状态栏颜色
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);//隐藏状态栏
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);//沉浸式状态栏
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.BLUE.getValue());    //状态栏颜色
    }

    private void askPermission() {
        List<String> permissionList = new ArrayList<>();
        for (String s : permission) {
            if (verifySelfPermission(s) != 0 && canRequestPermission(s)) {
                permissionList.add(s);
            }
        }
        if (permissionList.size() > 0) {
            requestPermissionsFromUser(permissionList.toArray(new String[0]), 0);
        }
    }

}
