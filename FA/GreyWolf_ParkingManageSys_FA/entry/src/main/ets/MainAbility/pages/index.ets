/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// 导入导航栏
import TabsNavigation from '../common/TabsNavigation'
import router from '@system.router';
import prompt from '@system.prompt';
import ParkingApi from '@ohos.ParkingApi'
import CommonLog from "../common/CommonLog"
import StorageUtils from '../common/StorageUtils'
import {RecordInfo} from './accessRecords';
import emitter from '@ohos.events.emitter'
import Notification from '@ohos.notification';
import featureAbility from '@ohos.ability.featureAbility';


const TAG = 'index'

function controlLatch(group: string, operate: 'open' | 'close') {
    // 底层有判断在线离线状态
    CommonLog.info(TAG, `start controlLatch operate=${operate} ,group=${group}`)
    ParkingApi.controlLatch(group, operate).then((res) => {
        CommonLog.info(TAG, `ParkingApi.controlLatch callback argument ${group},${operate}:${JSON.stringify(res)}`)
        // 日志
        //        if (res.state == ResponseState.ERROR) {
        //            prompt.showToast({ message: `${operate} latch fail: ${JSON.stringify(res)}` })
        //        } else {
        //            prompt.showToast({ message: `${operate} latch success:${JSON.stringify(res)}` })
        //        }
    }).catch(err => {
        CommonLog.info(TAG, 'controlLatch error:' + JSON.stringify(err))
        prompt.showToast({ message: err })
    })
}

function publishCarCount(group){
    if (!group) {
        return
    }
    //通知Request对象
    let notificationRequest = {
        id: 1,
        content: {
            contentType: Notification.ContentType.NOTIFICATION_CONTENT_BASIC_TEXT,
            normal: {
                title: "来车通知",
                text: group[0] + "门来车了",
                additionalText: ""
            }
        }
    }
    Notification.publish(notificationRequest, 1, ()=>{});
    let eventData = {
        data: {
            group: group
        }};
    let innerEvent = {
        eventId: 1,
        priority: emitter.EventPriority.HIGH
    };
    CommonLog.info(TAG, `publishCarCount emitter group=${group},${JSON.stringify(emitter.emit)}`)
        emitter.emit(innerEvent, eventData);
    CommonLog.info(TAG, `publishCarCount finish`)

}

/**
 * 首页
 */
@Entry
@Component
struct Index {
    private controller: TabsController = new TabsController()
    @State tabIndex: number = 0
    private groups: Array<string> = ['A', 'B', 'C', 'D']
    private deviceList: Array<DeviceInfo> = []
    @State groupAEntrance: Array<DeviceInfo> = []
    @State groupAExit: Array<DeviceInfo> = []
    @State groupBEntrance: Array<DeviceInfo>  = []
    @State groupBExit: Array<DeviceInfo>  = []
    @State groupCEntrance: Array<DeviceInfo>  = []
    @State groupCExit: Array<DeviceInfo>  = []
    @State groupDEntrance: Array<DeviceInfo>  = []
    @State groupDExit: Array<DeviceInfo>  = []
    private updateTimer: number

    build() {
        Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Start }) {
            Flex({
                direction: FlexDirection.Row,
                alignItems: ItemAlign.Center,
                justifyContent: FlexAlign.SpaceBetween
            }) {
                Text().width(100)
                Text('停车场管理系统').fontColor(Color.White).fontSize(32)
                Text('门禁记录').fontColor('#4E98FF').fontSize(24).width(100)
            }.margin({ left: 16, right: 16 }).onClick(() => {
                router.push({ uri: 'pages/accessRecords' })
            }).height(130)

            TabsNavigation({ tabIndex: $tabIndex, groups: this.groups, controller: this.controller })
            // 设置页面位于顶部
            Tabs({ barPosition: BarPosition.Start, index: 0, controller: this.controller }) {
                TabContent() {
                    Column(){
                        EntranceCard({ group: 'A_ENTRANCE', tabDeviceList: $groupAEntrance })
                        EntranceCard({ group: 'A_EXIT', tabDeviceList: $groupAExit })
                    }.onClick(()=>{
                        this.saveLicensePlate("licensePlate", "A_ENTRANCE")
                    })
                }.tabBar('A').margin({ left: 16, right: 16 })

                TabContent() {
                    Column(){
                        EntranceCard({ group: 'B_ENTRANCE', tabDeviceList: $groupBEntrance })
                        EntranceCard({ group: 'B_EXIT', tabDeviceList: $groupBExit })
                    }
                }.tabBar('B').margin({ left: 16, right: 16 })

                TabContent() {
                    Column(){
                        EntranceCard({ group: 'C_ENTRANCE', tabDeviceList: $groupCEntrance })
                        EntranceCard({ group: 'C_EXIT', tabDeviceList: $groupCExit })
                    }
                }.tabBar('C').margin({ left: 16, right: 16 })

                TabContent() {
                    Column(){
                        EntranceCard({ group: 'D_ENTRANCE', tabDeviceList: $groupDEntrance })
                        EntranceCard({ group: 'D_EXIT', tabDeviceList: $groupDExit })
                    }
                }.tabBar('D').margin({ left: 16, right: 16 })
            }
            .vertical(false)
            .scrollable(true)
            .barMode(BarMode.Fixed)
            .barWidth(400)
            .barHeight(0)
            .animationDuration(400)
            .onChange((index: number) => {
                this.tabIndex = index
            })
        }
        .backgroundColor($r("app.color.backgroundColor"))
        .width('100%')
        .height('100%')
    }

    aboutToAppear() {
        CommonLog.info(TAG, "aboutToAppear")
        featureAbility.getContext().getOrCreateLocalDir((err, data) => {
            CommonLog.info(TAG, "create log file success" + data)
            data += "/files"
            ParkingApi.init(data).then((result) => {
                CommonLog.info(TAG, `ParkingApi init success :${JSON.stringify(result)}`)
                try {
                    this.setDeviceUpdate();
                } catch (error) {
                    CommonLog.error(TAG, "DeviceUpdate registered fail:" + error)
                }

                try {
                    this.setListenCarAccess();
                } catch (error) {
                    prompt.showToast({ message: "ListenCarAccess registered fail:" + error })
                    CommonLog.error(TAG, "ListenCarAccess registered fail:" + error)
                }
            });
        })
        this.tabIndex = 0
    }

    aboutToDisappear() {
        CommonLog.info(TAG, `aboutToDisappear start clearInterval`)
        clearInterval(this.updateTimer)
        CommonLog.info(TAG, `aboutToDisappear release`)
        ParkingApi.release().then((result) => {
            CommonLog.info(TAG, `ParkingApi release success :${JSON.stringify(result)}`)
        });
    }

    setDeviceUpdate() {
        CommonLog.info(TAG, `setDeviceUpdate start`)
        ParkingApi.on('devicesUpdate', (res) => {
            CommonLog.info(TAG, `setDeviceUpdate callback`)
            try {
                CommonLog.info(TAG, `devicesUpdate res = ${JSON.stringify(res.devicesUpdate)}`)
                this.deviceList = res.devicesUpdate
                this.initTabDeviceList()
            } catch (error) {
                CommonLog.info(TAG, '===5=== ParkingSystem devicesUpdate error:' + JSON.stringify(error));
            }
        }).catch(err => {
            CommonLog.info(TAG, `ParkingSystem ===5=== set devicesUpdate error:${JSON.stringify(err)}`)
        })
        CommonLog.info(TAG, `setDeviceUpdate end`)
    }

    setListenCarAccess() {
        //注册carAccess
        CommonLog.info(TAG, `start setListenCarAccess`)
        ParkingApi.on('carAccess', (licensePlate, group, state) => {
            CommonLog.info(TAG, `setListenCarAccess callback licensePlate:${licensePlate},k state:${state}, group:${group}`)
            if (state == 'waiting') {
                CommonLog.info(TAG, `========setListenCarAccess in waiting`)
                this.saveLicensePlate(licensePlate, group)
            } else if (state == 'leave') {
                // 获取同组latch
                CommonLog.info(TAG, `========setListenCarAccess leave`)
                try {
                    controlLatch(group, 'close')
                } catch (error) {
                    CommonLog.error(TAG, 'controlLatch close error:' + JSON.stringify(error));
                }
            }
        }).catch(err => {
            CommonLog.error(TAG, `setListenCarAccess error:${JSON.stringify(err)}`)
        })
    }

    getLatchListByGroup(group): Array<DeviceInfo>{
        let latchList = []
        this.deviceList.forEach(item => {
            if (item.group == group && item.deviceType == 'latch') {
                latchList.push(item)
            }
        })
        CommonLog.info(TAG, `getLatchListByGroup = ${JSON.stringify(latchList)}`)
        return latchList
    }

    async saveLicensePlate(licensePlate, group: string) {
        if (!group) {
            CommonLog.error(TAG, `error saveLicensePlate group=${group}`)
            return
        }
        CommonLog.info(TAG, 'licensePlate res :' + licensePlate)


        try {
            CommonLog.info(TAG, 'start saveLicensePlate')
            // 保存车牌记录
            let res = await StorageUtils.getRecordList()
            let recordList: Array<RecordInfo> = JSON.parse(res)
            // 查看是
            let record = recordList.find(item => {
                if (item.licensePlate == licensePlate && !item.exit && group.includes('EXIT')) {
                    return true
                }
                return false
            })
            CommonLog.info(TAG, 'saveLicensePlate record:' + JSON.stringify(record))
            if (record) {
                let now = new Date();
                let before = new Date(record.enterTime)
                record.leaveTime = now.toString();
                record.exit = group[0]
                let timePrice = await StorageUtils.getPrice()
                let hour = Math.ceil((now.getTime() - before.getTime()) / 3600000)
                record.price = timePrice * hour + ''
            } else {
                record = new RecordInfo(licensePlate, group[0], '', new Date().toString(), '', '')
                recordList.push(record)
            }
            //            // 保存车牌记录
            await StorageUtils.setRecordList(recordList)
            let notificationRequest = {
                id: 1,
                content: {
                    contentType: Notification.ContentType.NOTIFICATION_CONTENT_BASIC_TEXT,
                    normal: {
                        title: "来车通知",
                        text: group[0] + "门来车了",
                        additionalText: ""
                    }
                }
            }
            CommonLog.info(TAG, `publishCarCount 333`)
            await Notification.publish(notificationRequest, 1);
            //             出入量+1
            await StorageUtils.saveCardCount(group)
            //             通知更新出入量
            publishCarCount(group)
            CommonLog.info(TAG, 'saveLicensePlate success')
            controlLatch(group, 'open')
            prompt.showToast({message:"保存车牌并开门成功！"})
        } catch (error) {
            CommonLog.error(TAG, 'open error' + JSON.stringify(error));
        }
        CommonLog.info(TAG, 'saveLicensePlate finish')

    }

    async initTabDeviceList() {
        CommonLog.info(TAG, `initTabDeviceList deviceList : ${JSON.stringify(this.deviceList)}`)

        let groupHandle = (entranceList, exitList, item:DeviceInfo)=> {
            if (item.group.includes('ENTRANCE')) {
                let hasDevice = false
                entranceList.forEach(entrance=>{
                    if(entrance.id == item.id){
                        hasDevice = true
                        if(entrance&&item&&entrance.state != item.state){
                            entrance.state = item.state
                        }
                    }
                })
                if (!hasDevice) {
                    entranceList.push(item)
                }
                CommonLog.info(TAG,`groupHandle：entranceList=${JSON.stringify(entranceList)}`)
            } else {
                let hasDevice = false
                exitList.forEach(exit=>{
                    if(exit.id == item.id){
                        hasDevice = true
                        exit = item
                    }
                })
                if (!hasDevice) {
                    exitList.push(item)
                }else{
                    exitList = JSON.parse(JSON.stringify(exitList))
                }
                CommonLog.info(TAG,`groupHandle：exitList=${JSON.stringify(exitList)}`)
            }
        }

        this.deviceList.forEach(item => {
            switch (item.group[0]) {
                case 'A':
                {
                    groupHandle(this.groupAEntrance, this.groupAExit, item)
                    break;
                }
                case 'B':
                {
                    groupHandle(this.groupBEntrance, this.groupBExit, item)
                    break;
                }
                case 'C':
                {
                    groupHandle(this.groupCEntrance, this.groupCExit, item)
                    break;
                }
                case 'D':
                {
                    groupHandle(this.groupDEntrance, this.groupDExit, item)
                    break;
                }
            }
        })
    }

    resetDeviceData() {
        this.deviceList = []
        this.groupAExit = []
        this.groupAEntrance = []
        this.groupBEntrance = []
        this.groupBExit = []
        this.groupCEntrance = []
        this.groupCExit = []
        this.groupDEntrance = []
        this.groupDExit = []
    }
}


// 设备状态值
type deviceState = 'online' | 'offline';
type latchOperate = 'open' | 'close';

// 设备信息
export class DeviceInfo {
    id: string;
    name: string ;
    deviceType: string;
    state: deviceState;
    group: string;

    constructor(id: string, name: string, deviceType: string, state: deviceState, group: string) {
        this.id = id;
        this.name = name;
        this.deviceType = deviceType;
        this.state = state;
        this.group = group;
    }
}

export enum ResponseState {
    /**
         * 控制命令响应状态值.
         */
    SUCCESS = 200,
    ERROR = 500
}

@Component
struct EntranceCard {
    @Link tabDeviceList: Array<DeviceInfo>
    @State carCount: number = 0
    private entranceType: string = 'ENTRANCE'
    private doorway: string = 'A'
    private group: string = 'A_ENTRANCE'
    licensePlateController: CustomDialogController = new CustomDialogController({
        builder: licensePlateDialog({ confirm: (licensePlate) => this.submit(licensePlate) }),
        autoCancel: true
    })
    @State selectedIndex:number = 0
    @State selectedDevice:DeviceInfo = undefined

    @Builder pixelMapBuilder() {
        Column() {
            Image(this.getDeviceImage(this.selectedDevice.deviceType, this.selectedDevice.state))
                .width(96)
                .height(96)
        }
    }

    build() {
        Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.SpaceBetween }) {
            Row() {
                Text('')
                    .width(4)
                    .height(28)
                    .backgroundColor(this.isEntrance() ? $r("app.color.entrance") : $r("app.color.exit"))
                    .margin({ right: 12 })
                Text(this.isEntrance() ? '入口' : '出口').fontColor(Color.White).fontSize(28)
            }

            if (this.tabDeviceList && this.tabDeviceList.length > 0) {
                List({space:110,initialIndex:0}) {
                    ForEach(this.tabDeviceList, (item: DeviceInfo,index:number) => {
                        ListItem(){
                            Flex({direction:FlexDirection.Column,justifyContent:FlexAlign.Center,alignItems:ItemAlign.Center}) {
                                Image(this.getDeviceImage(item.deviceType, item.state))
                                    .width(96)
                                    .height(96)
                                    .margin({ bottom: 16 })
                                    .onDragStart((event: DragEvent, extraParams: string) => {
                                        this.selectedDevice = this.tabDeviceList[index]
                                        this.selectedIndex = index
                                        return this.pixelMapBuilder()
                                    })
                                Text(item.name).fontColor('#FFFFFF').fontSize(24)
                                Text(item.state).fontColor('#FFFFFF').fontSize(24)
                            }


                        }
                    }, item => item.id)

                }.listDirection(Axis.Horizontal)
                .height('70%')
                .onDrop((event: DragEvent, extraParams: string) => {
                    var jsonString = JSON.parse(extraParams)
                    CommonLog.info(TAG,"onDrop111"+ extraParams)
                    this.tabDeviceList.splice(this.selectedIndex, 1)
                    this.tabDeviceList.splice(jsonString.insertIndex, 0, this.selectedDevice)
                })

            } else {
                Image($r("app.media.empty_device")).height(140).objectFit(ImageFit.Contain)
                Text('暂无在线设备').fontColor('#FFFFFF').fontSize(28).width('100%').textAlign(TextAlign.Center)
            }
            Row() {
                Image(this.isEntrance() ? $r("app.media.entrance") : $r("app.media.exit"))
                    .width(32)
                    .height(32)
                    .margin({ right: 12 })
                Text(this.isEntrance() ? '驶进车辆:' : '驶出车辆:')
                    .fontColor(this.isEntrance() ? $r("app.color.entrance") : $r("app.color.exit"))
                    .fontSize(24)
                Text(this.carCount + '')
                    .fontColor(this.isEntrance() ? $r("app.color.entrance") : $r("app.color.exit"))
                    .fontSize(24)
                    .margin({ left: 16 })
            }.width('100%').visibility(this.hasLatch()?Visibility.Visible:Visibility.None)

            Flex({ direction: FlexDirection.Row, justifyContent: FlexAlign.Center }) {
                Button('打开门禁')
                    .width(168)
                    .height(64)
                    .backgroundColor('#5D6785')
                    .fontSize(24)
                    .fontColor($r("app.color.entrance") )
                    .onClick(() => {
                        this.licensePlateController.open()
                    })
                Button('关闭门禁')
                    .width(168)
                    .height(64)
                    .backgroundColor('#5D6785')
                    .fontSize(24)
                    .margin({left:20})
                    .fontColor($r("app.color.exit"))
                    .onClick(() => {
                        controlLatch(this.group,'close')
                        prompt.showToast({message:'关闭门禁完成'})
                    })
            }.width('100%').visibility(this.hasLatch()?Visibility.Visible:Visibility.None)
        }
        .width('100%')
        .height(420)
        .backgroundColor($r('app.color.cardBackground'))
        .borderRadius(16)
        .padding(24)
        .margin({ top: 16, bottom: 16 })
    }

    async aboutToAppear() {
        let arr = this.group.split('_')
        if (arr.length < 2) {
            CommonLog.error(TAG, `EntranceCard group error`)
            return
        }
        this.doorway = arr[0]
        this.entranceType = arr[1]
        this.carCount = await StorageUtils.getCarCount(this.group)

        //创建订阅者
        emitter.on({ eventId: 1 }, (eventData) => {
            let group = eventData.data.group
            if(group != this.group){
                return
            }
            this.carCount++
        });
    }

    getDeviceImage(deviceType, deviceState: deviceState): Resource | string{
        if (deviceState == "online") {
            switch (deviceType) {
                case 'latch':
                    return $r("app.media.latch_online")
                case 'trigger':
                    return $r("app.media.trigger_online")
                case 'camera':
                    return $r("app.media.camera_online")
            }
        } else {
            switch (deviceType) {
                case 'latch':
                    return $r("app.media.latch_offline")
                case 'trigger':
                    return $r("app.media.trigger_offline")
                case 'camera':
                    return $r("app.media.camera_offline")
            }
        }
        return ''
    }

    isEntrance(): boolean{
        if (this.entranceType.includes('ENTRANCE')) {
            return true
        } else {
            return false
        }
    }

    async submit(licensePlate) {
        CommonLog.info(TAG, 'licensePlate' + licensePlate)
        // 保存车牌记录
        let res = await StorageUtils.getRecordList()
        let recordList: Array<RecordInfo> = JSON.parse(res)
        let record = recordList.find(item => {
            console.info(JSON.stringify(item))
            if (item.licensePlate == licensePlate && !this.isEntrance() && !item.exit) {
                return true
            }
            return false
        })
        if (record) {
            let now = new Date();
            let before = new Date(record.enterTime)
            record.leaveTime = now.toString();
            record.exit = this.doorway
            let timePrice = await StorageUtils.getPrice()
            let hour = Math.ceil((now.getTime() - before.getTime()) / 3600000)
            record.price = timePrice * hour + ''
        } else {
            record = new RecordInfo(licensePlate, this.doorway, '', new Date().toString(), '', '')
            recordList.push(record)
        }

        StorageUtils.setRecordList(recordList)
        // 出入量+1
        StorageUtils.saveCardCount(this.group)
        CommonLog.info(TAG, 'start publishCarCount')
        // 通知更新出入量
        publishCarCount(this.group)
        controlLatch(this.group, 'open')
    }

    hasLatch() {
        let item = this.tabDeviceList.find(item => {
            return item.deviceType == 'latch'
        })
        return item != undefined
    }
}

@CustomDialog
struct licensePlateDialog {
    @State licensePlate: string = ''
    controller: CustomDialogController
    confirm: (licensePlate) => void

    build() {
        Column() {
            Text('手动开闸').fontSize(36).fontColor($r("app.color.fontMain")).margin({ bottom: 30 })
            Flex({
                direction: FlexDirection.Row,
                justifyContent: FlexAlign.SpaceBetween,
                alignItems: ItemAlign.Center
            }) {
                Text('请输入车牌 粤').fontSize(28).fontColor($r("app.color.fontMain"))
                TextInput()
                    .placeholderColor($r("app.color.fontMain"))
                    .fontColor($r("app.color.fontMain"))
                    .fontSize(24)
                    .type(InputType.Normal)
                    .flexGrow(1)
                    .width(100)
                    .onChange((value) => {
                        this.licensePlate = value
                    })
                    .onCopy(value=>{
                        CommonLog.info(TAG,`copy success:${value}`)
                        prompt.showToast({message:`复制成功:${value}`})
                    })
                    .onCut(value=>{
                        CommonLog.info(TAG,`cut success::${value}`)
                        prompt.showToast({message:`剪切成功:${value}`})
                    })
                    .onPaste(value=>{
                        CommonLog.info(TAG,`paste success::${value}`)
                        prompt.showToast({message:`粘贴成功:${value}`})
                    })
            }.margin({ bottom: 20 })

            Flex({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceAround }) {
                Button('确定')
                    .width(128)
                    .height(48)
                    .backgroundColor('#5D6785')
                    .fontSize(24)
                    .onClick(() => {
                        this.confirm('粤' + this.licensePlate)
                        this.controller.close()
                    })
                Button('取消')
                    .width(128)
                    .height(48)
                    .backgroundColor('#5D6785')
                    .fontSize(24)
                    .onClick(() => {
                        this.controller.close()
                    })
            }
        }.width('100%').backgroundColor('#030514').padding(32)
    }

    setLicensePlate() {

    }
}

