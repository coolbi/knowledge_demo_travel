# vue-web
https://openlayers.org/en/latest/examples/layer-swipe.html
https://openlayers.org/en/latest/examples/draw-and-modify-features.html



## Build Setup

``` bash
test
Call Gaode map to show the places you have been and the path you have traveled (run in the browser and can be packaged with H5 for APP)

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
