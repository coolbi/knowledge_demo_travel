// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import $ from 'jquery';
// import Icon from 'vue2-svg-icon/Icon'
// Vue.component('icon', Icon)
import SvgIcon from 'vue-svgicon'

Vue.use(SvgIcon, {
  tagName: 'svg-icon'
});
import { Search } from 'vant';
import { Icon } from 'vant';
Vue.use(Search);
Vue.use(Icon);

import * as api from './js/api.js'
Vue.prototype.api = api;

Vue.use(ElementUI);
window.$ = $

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})


$(".drag").on("dragstart", function (e) {
  window.offsety = e.pageY - $(this).parent().offset().top;
  window.offsetx = e.pageX - $(this).parent().offset().left;
});
$(".drag").on("dragover", function (e) {
  var parent = $(this).parent();
  parent.css({"left": (e.pageX -window.offsetx) + 'px', "top": (e.pageY-window.offsety) + "px"});
})

