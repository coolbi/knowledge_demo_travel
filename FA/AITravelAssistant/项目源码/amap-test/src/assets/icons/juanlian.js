/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'juanlian': {
    width: 200,
    height: 200,
    viewBox: '0 0 1024 1024',
    data: '<defs/><path pid="0" d="M176 864h672v64H176z"/><path pid="1" d="M176 768h48v160h-48zm624 0h48v160h-48zm0-136h48v64h-48zm-624 0h48v64h-48zm624-144h48v64h-48zm-624 0h48v64h-48zm624-152h48v64h-48zm-624 0h48v64h-48zm624-144h48v64h-48zm-624 0h48v64h-48zM88 96h848q24 0 24 24t-24 24H88q-24 0-24-24t24-24z"/><path pid="2" d="M88 240h848q24 0 24 24t-24 24H88q-24 0-24-24t24-24zM88 384h848q24 0 24 24t-24 24H88q-24 0-24-24t24-24zM88 528h848q24 0 24 24t-24 24H88q-24 0-24-24t24-24zM88 672h848q24 0 24 24t-24 24H88q-24 0-24-24t24-24zM880 816a32 32 0 1064 0 32 32 0 10-64 0z"/>'
  }
})
