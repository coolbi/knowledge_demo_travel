/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'point': {
    width: 200,
    height: 200,
    viewBox: '0 0 1024 1024',
    data: '<defs/><path pid="0" d="M512 320a192.064 192.064 0 010 384 192 192 0 010-384z"/>'
  }
})
