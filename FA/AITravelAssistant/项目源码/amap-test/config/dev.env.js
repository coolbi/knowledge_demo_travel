'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  ARCGIS_CSS : '"http://218.85.80.194:12324/ags410/library/4.10/esri/themes/light/main.css"'
})
