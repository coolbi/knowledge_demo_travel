import router from '@system.router';

export default {
    data: {
        time:"点击选择天数",
        timeVal:"",
        times:['三天','四天','五天'],

        busy:"点击选择行程紧凑程度",
        busyVal:"",
        busys:['悠闲','适中','紧凑'],

        city:"点击选择出行城市",
        cityVal:"",
        citys:["北京","上海","重庆","天津","厦门","福州","泉州","南平","宁德","杭州","宁波","台州","湖州","南京","苏州","扬州","无锡","连云港","南通","青岛","济南","合肥","黄山","大连","沈阳","石家庄","哈尔滨","太原","呼和浩特","呼伦贝尔","长春","三亚","海口","广州","深圳","珠海","长沙","武汉","宜昌","成都","昆明","大理","宝鸡","拉萨","兰州","乌鲁木齐","遵义","银川"],
    },
    ////////////城市选择器////////////
    chooseCity(e) {
        this.city = e.newValue;
        this.cityVal = e.newSelected;
    },
    ////////////天数选择器////////////
    chooseTime(e) {
        this.time = e.newValue;
        this.timeVal = e.newSelected;
    },
    ////////////紧凑程度选择器////////////
    chooseBusy(e) {
        this.busy = e.newValue;
        this.busyVal = e.newSelected;
    },

    ////////////点击按钮将所选城市、天数、紧凑程度以及个人喜好传给下一页面////////////
    SHOW(){
        router.push({
            uri: "pages/choo/choo",
            params: {
                occupation: this.occupation,
                age: this.age,
                gender: this.gender,
                region: this.region,
                genre: this.genre,
                time : this.timeVal,
                busy : this.busyVal,
                city : this.cityVal,
            } //params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
        })
    },
    ////////////页面初始化，跳转页面时自动触发////////////
    onInit(){
    },
    ////////////点击注册按钮，跳转至注册页面////////////
    Jumppages(){
        router.push({
            uri: "pages/login/login",
        });
    }
}
