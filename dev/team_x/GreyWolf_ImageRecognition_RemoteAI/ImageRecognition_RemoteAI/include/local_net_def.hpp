/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LOCAL_NET_DEF_H
#define LOCAL_NET_DEF_H

extern "C" { 
#include <iostream>
#include <hilog_wrapper.h>

#define LOG_D(fmt, ...) 
#define LOG_W(fmt, ...) 
#define LOG_I(fmt, ...) 
#define LOG_E(fmt, ...) 
#define DBG_ASSERT(cond)                                                            \
        do {                                                                             \
            if (!(cond)) {                                                               \
                LOG_E("'%s' assert failed.\r\n", #cond); \
                while (1)                                                                \
                    ;                                                                    \
            }                                                                            \
        } while (0)
#define SYSERR(x,option,y,message,z)
}

#endif

