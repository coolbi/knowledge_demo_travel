
/*
# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/

#ifndef __APP_H__
#define __APP_H__

using namespace std;

/**
 * @brief GetRecognizePoints:get the frame of the face from the pictrue.
 * 人脸框获取API
 * 
 * @param image_path the pictrue to Recognize
 * 参数需要传入一个图片用来识别人脸框
 *
 * @returns the Recognized array. which contained the coord.exp:[x, y, w, h]
 * 返回人脸框的数组，主要包含坐标和长宽[x,y,w,h]
 */
int GetRecognizePoints(const char *image_path);

/**
 * @brief FaceSearchInit
 * 人脸搜索识别 初始化
 * @param
 *
 * @returns success 0, failed -1
 */
int FaceSearchInit();

/**
 * @brief FaceSearchDeinit
 * 人脸搜索识别 逆初始化
 * @param
 *
 * @returns
 */
void FaceSearchDeinit();

/**
 * @brief FaceSearchRegister 
 * 人脸搜索识别注册人脸
 * 
 * @param value the name && the image to save. it's a json object.exp:{"name":"刘德华","sum":"2","image":{"11.jpg","12.jpg"}}
 * 参数为一个json格式的对象,包含注册人脸的名字，人脸图片的个数以及图片名称。如{"name":"liudehua","sum":"2","image":{"path":"11.jpg","path":"12.jpg"}}
 *
 * @returns success 0, failed -1
 */
int FaceSearchRegister(const char *value);

/**
 * @brief FaceSearchGetRecognize recognize the image
 * 获取人脸搜索识别结果
 * @param image_path the image to recognize
 *参数为需要识别的图片
 * @returns success: the result of recognize. if success return the name of the image;
 * 返回识别接口，识别OK的话，返回人名。
 */
char *FaceSearchGetRecognize(const char *image_path);

#endif
