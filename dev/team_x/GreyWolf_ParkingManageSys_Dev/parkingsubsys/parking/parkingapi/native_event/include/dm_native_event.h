/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// #ifdef __cplusplus 
// extern "C++" { 
// #endif

// #ifndef OHOS_DM_NATIVE_EVENT_H
// #define OHOS_DM_NATIVE_EVENT_H

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <stdio.h>
#include <string.h>

#include "napi/native_api.h"

struct DmEventListener {
    std::string eventType;
    int intervalTime;
    napi_ref handlerRef = nullptr;
};

class DmNativeEvent {
public:
    DmNativeEvent(napi_env env, napi_value thisVar);
    virtual ~DmNativeEvent();

     /**
     * @brief 监听是否有车辆进入（1. 来自摄像头云端识别 2.来自自己本地识别），触发回调方式：
     *        1.车辆来了，有摄像头则获取到车牌识别结果后进入回调（携带识别结果），无则感应到车辆靠近则触发（无识别结果）
     *        2.车辆离开就触发回调。
     * @param eventType carAccess
     * @param handler 回调
     */
    virtual void On(std::string &eventType, napi_value handler);

    /**
     * @brief 订阅所有设备设备更新状态
     * @param eventType devicesUpdate
     * @param handler 回调
     * @param intervalTime 可选 更新列表间隔时间 默认值 3s
     */
    virtual void On(std::string &eventType, napi_value handler, int intervalTime);

    /**
     * @brief 取消车辆出入监听 + 取消所有设备设备更新状态监听
     * 
     * @param eventType carAccess、devicesUpdate
     * @param handler 所需注销事件
     */
    virtual void Off(std::string &eventType);

    /**
     * @brief 响应事件监听
     * 
     * @param eventType carAccess、devicesUpdate
     * @param argc 
     * @param argv 
     */
    virtual void OnEvent(const std::string &eventType, size_t argc, const napi_value *argv);

protected:
    napi_env env_;
    //napi_ref thisVarRef_;
    napi_ref thisVarRef_ = nullptr;
    std::map<std::string, std::shared_ptr<DmEventListener>> eventMap_;
};

// #endif /* OHOS_DM_NATIVE_EVENT_H */

// #ifdef __cplusplus 
// } 
// #endif 