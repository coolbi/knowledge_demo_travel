/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hilog_wrapper.h"
#include "local_net_utils.h"

#include "../include/dm_native_event.h"
#include "../../local_net/include/local_net_udp.h"

#ifdef  LOG_E
#undef  LOG_E
#endif
#define LOG_E   HILOG_ERROR

#ifdef  LOG_I
#undef  LOG_I
#endif
#define LOG_I   HILOG_INFO

DmNativeEvent::DmNativeEvent(napi_env env, napi_value thisVar)
{
    env_ = env;
    napi_create_reference(env, thisVar, 1, &thisVarRef_);
}

DmNativeEvent::~DmNativeEvent()
{
    for (auto iter = eventMap_.begin(); iter != eventMap_.end(); iter++) {
        std::shared_ptr<DmEventListener> listener = iter->second;
        napi_delete_reference(env_, listener->handlerRef);
    }
    eventMap_.clear();
    napi_delete_reference(env_, thisVarRef_);
}

void DmNativeEvent::On(std::string &eventType, napi_value handler)
{
    LOG_I("DmNativeEvent On in for event: %s", eventType.c_str());
    std::shared_ptr<DmEventListener> listener = std::make_shared<DmEventListener>();
    listener->eventType = eventType;
    napi_create_reference(env_, handler, 1, &listener->handlerRef);
    eventMap_[eventType] = listener;
}

void DmNativeEvent::On(std::string &eventType, napi_value handler, int intervalTime)
{
    LOG_I("DmNativeEvent On in for event: %s", eventType.c_str());
    std::shared_ptr<DmEventListener> listener = std::make_shared<DmEventListener>();
    listener->eventType = eventType;
    listener->intervalTime = intervalTime;
    napi_create_reference(env_, handler, 1, &listener->handlerRef);
    eventMap_[eventType] = listener;
}

void DmNativeEvent::Off(std::string &eventType)
{
    LOG_I("DmNativeEvent Off in for event: %s", eventType.c_str());
    napi_handle_scope scope = nullptr;
    napi_open_handle_scope(env_, &scope);
    if (scope == nullptr) {
        LOG_E("scope is nullptr");
        return;
    }

    auto iter = eventMap_.find(eventType);
    if (iter == eventMap_.end()) {
        LOG_E("eventType %s not find", eventType.c_str());
        return;
    }
    std::shared_ptr<DmEventListener> listener = iter->second;
    napi_delete_reference(env_, listener->handlerRef);
    eventMap_.erase(eventType);
    napi_close_handle_scope(env_, scope);
}

void DmNativeEvent::OnEvent(const std::string &eventType, size_t argc, const napi_value *argv)
{
    LOG_I("OnEvent for %{public}s", eventType.c_str());
    addLogs("ParkingSystem   OnEvent for:  \n");
    addLogs(eventType.c_str());

    napi_handle_scope scope = nullptr;
    napi_open_handle_scope(env_, &scope);
    if (scope == nullptr) {
        LOG_E("scope is nullptr");
        addLogs("==ParkingSystem    error scope is nullptr! \n");
        return;
    }
    LOG_I("==ParkingSystem %d \r\n", __LINE__);
    auto iter = eventMap_.find(eventType);
    if (iter == eventMap_.end()) {
        LOG_E("eventType %s not find", eventType.c_str());
        addLogs("==ParkingSystem    eventType %{public}s not find: \n");
        addLogs(eventType.c_str());
        return;
    }
    LOG_I("==ParkingSystem %d \r\n", __LINE__);
    std::shared_ptr<DmEventListener> listener = iter->second;
    napi_value thisVar = nullptr;
    napi_status status = napi_get_reference_value(env_, thisVarRef_, &thisVar);
    if (status != napi_ok) {
        addLogs("napi_get_reference_value thisVar for  failed");
        LOG_E("napi_get_reference_value thisVar for %s failed, status=%d", eventType.c_str(), status);
        return;
    }
    LOG_I("==ParkingSystem %d \r\n", __LINE__);
    napi_value handler = nullptr;
    status = napi_get_reference_value(env_, listener->handlerRef, &handler);
    if (status != napi_ok) {
        addLogs("napi_get_reference_value handler for failed");
        LOG_E("napi_get_reference_value handler for %s failed, status=%d", eventType.c_str(), status);
        return;
    }
    LOG_I("==ParkingSystem %d \r\n", __LINE__);
    napi_value callResult = nullptr;
    status = napi_call_function(env_, thisVar, handler, argc, argv, &callResult);
    if (status != napi_ok) {
        LOG_E("napi_call_function for %s failed, status=%d", eventType.c_str(), status);
        addLogs("napi_call_function forfailed: ");
        addLogs(eventType.c_str());
        return;
    }
    LOG_I("==ParkingSystem ==OnEvent==ok== \r\n");
    addLogs("==OnEvent==ok==");
    napi_close_handle_scope(env_, scope);
    LOG_I("==ParkingSystem ==OnEvent==end== \r\n");
}
