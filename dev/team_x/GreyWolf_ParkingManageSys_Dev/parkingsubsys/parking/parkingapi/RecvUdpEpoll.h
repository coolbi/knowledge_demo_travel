/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <cJSON.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <deque>

#include "local_net/include/local_net_def.h"

#define IPADDRESS   "127.0.0.1"
#define PORT        50000
#define MAXSIZE     1024
#define LISTENQ     5
#define FDSIZE      1000
#define EPOLLEVENTS 100

// 设备数据
class DeviceInfo {
  public:
    /**
     * 设备名
     */
    std::string name;

    /**
    * 设备类型 避免和关键字type冲突
    */
    std::string deviceType;

    /**
     * 设备id
     */
    std::string id;

    /**
     * 设备状态,设备状态值 'online' | 'offline'
     */
    std::string state;

    /**
    * 分组 取值类似 A_ENTRANCE B_EXIT  A/B/C/D + _ + ENTRANCE/EXIT
    */
    std::string group;

    /**
		* 上次发送udp时间
    */
    struct timeval time;
};

// 响应值
class Response {
  public:
    /**
     * 响应状态
     */
    ResponseState state;

    // 响应状态 online, offline
    std::string stateStr;

    /**
     * 设备状态
     */
    std::string result;

    /**
     * 响应时间
     */
    std::string time;
};

// 车辆信息
class CarInfo {
  public:
    /**
     * 车牌
     */
    std::string licensePlate;

    /**
     * 设备状态,车辆状态值 'entering' | 'leaving'
     */
    std::string carState;
};
