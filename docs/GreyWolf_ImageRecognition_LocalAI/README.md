# [车牌识别器（OpenCV版本）](../../dev/team_x/GreyWolf_ImageRecognition_LocalAI)

## 简介

本文介绍如何在OpenHarmony-3.1-Beta版本L1系统3516上[实现车牌识别](../../dev/team_x/GreyWolf_ImageRecognition_LocalAI)。语音播放、相机拍照、opencv识别车牌、获取识别结果并通过串口显示。

![](media/display.gif)

## 快速上手

### 硬件准备

润和Hi3516DV300 AI Camera 。

准备8GSD卡插入Hi3516DV300插槽。

### 下载源码

#### 准备

获取源码及Ubuntu编译环境准备

开发基础环境由windows 工作台和Linux  编译服务器组成。windows 工作台可以通过samba 服务或ssh  方式访问Linux编译服务器。其中windows  工作台用来烧录和代码编辑，Linux编译服务器用来编译OpenHarmony代码，为了简化步骤，Linux编译服务器推荐安装Ubuntu20.04。

[操作文档](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/device-dev/quick-start/quickstart-lite-env-setup.md#/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/device-dev/quick-start/quickstart-lite-env-setup-overview.md)

Hi3516开发环境准备

在Linux编译服务器上搭建好基础开发环境后，需要安装OpenHarmony 编译Hi3516平台特有的开发环境。

[操作文档](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3516.md)

#### 下载OpenHarmony源码

##### 码云工具下载

```
cd ~/
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > ./repo
sudo cp repo /usr/local/bin/repo
chmod a+x /usr/local/bin/repo
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests
```

##### 代码下载

[本地车牌识别demo](../../dev/team_x/GreyWolf_ImageRecognition_LocalAI)适配OpenHarmony-3.1-Beta版本。

新建文件夹

```
mkdir ~/OpenHarmony
cd ~/OpenHarmony
```

OpenHarmony-3.1-Beta下载：

```
repo init -u git@gitee.com:openharmony/manifest.git -b OpenHarmony-3.1-Beta --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

下载GreyWolf_ImageRecognition_LocalAI代码

具体仓库地址: [knowledge_demo_temp](https://gitee.com/LLLLLLin/knowledge_demo_temp)

通过git命令下载(方便后期代码上传管理，建议先将仓库fork到自己账号目录，然后再下载)：

```
git clone git@gitee.com:xxxxxxx/knowledge_demo_smart_home.git
其中xxxxxxx为fork后相关账号名字。
```

#### 代码拷贝

##### 拷贝

进入knowledge_demo_temp/knowledge_demo_smart_travel下

拷贝本地AI实现代码

```
cp -rf ImageRecognition_LocalAI/ ~/OpenHarmony/applications/sample/camera/ 
```

更改启动文件init_liteos_a_3518ev300.cfg将应用设置为开机自启。

```
cp -f config/init_liteos_a_3516dv300.cfg  ~/OpenHarmony/vendor/hisilicon/hispark_taurus/init_configs/init_liteos_a_3516dv300.cfg
```

更改appapplications.json与config.json将应用加入到编译体系中来

```
cp -f config/applications.json  ~/OpenHarmony/build/lite/components/applications.json
cp -f config/config.json ~/OpenHarmony/vendor/hisilicon/hispark_taurus/config.json
```

opencv移植

[L1上运行开源项目车牌识别及移植opencv库](../GreyWolf_EasyPR/readme.md)

### 编译

hb set 选择ipcamera_hispark_taurus

```
hb build -f  -- 开始全量编译。（hb build 为增量编译）
```

### 固件烧录

##### 烧录工具选择

固件编译完后，是需要烧录到单板的。这里我们用的是HiTool工具烧录的。(HiTool工具下载地址:[HiHope官网](https://gitee.com/link?target=http%3A%2F%2Fwww.hihope.org%2Fdownload%2Fdownload.aspx%3Fmtt%3D33))

##### 烧录步骤

打开HiTool工具，如下图：

烧写步骤按照图中标注即可。点击擦除后再拔出USB口再接入。![hitool_config](media/hitool_config.png)

最后重新上电即可烧录。

![](media/hitoo_burn.png)

烧录成功后，使用串口连接。输入如下启动参数。

```
setenv bootcmd "mmc read 0x0 0x80000000 0x800 0x4800; go 0x80000000";
setenv bootargs "console=ttyAMA0,115200n8 root=emmc fstype=vfat rootaddr=10M rootsize=60M rw";
save
reset
```

## 启动应用

#### 准备

将源码下的sdcard文件拷贝SD卡中。

在烧录前使用本地应用相机调整焦距，将摄像头调整识别二维码为距离为25cm。

#### 车牌识别

将摄像头对准车牌

![](media/licensePlate.png)

在串口中输入1拍照、输入2将照片提交至百度云进行识别，最终返回识别结果。

![](media/getLicensePlate.png)

## 样例联动

本样例由苍狼战队开发，可与其他智能停车场样例组合产品联动效果，参考 [智能停车场](../GreyWolf_SmartParking/README.md)