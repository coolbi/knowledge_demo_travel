# 本地车牌识别

### 一、简介

本文介绍如何在Openharmony L1 系统上运行开源项目--车牌识别(EasyPR)，该项目[github链接](https://github.com/liuruoze/EasyPR)，作者发布的[相关文章链接](https://www.cnblogs.com/subconscious/p/3979988.html);项目依赖于opencv这个开源库来实现的，下文将主要介绍如何移植opencv库和编译车牌识别库；本文不再介绍OpenHarmony L1的环境搭建，请确保hb build -f 能编译成功。



### 二、移植opencv

##### 1、获取源码

```
#将opencv库源码放在源码根目录下的third_party下
cd ~/openharmony/third_party
git clone https://github.com/opencv/opencv.git
```

##### 2、生成Makefile文件

 在opencv源码根目录新建build目录

```
cd opencv
mkdir build
```

使用 cmake-gui 来配置编译环境

```
cd build
cmake-gui ..
```

显示的UI界面如下图

&nbsp;![1](./1.jpg)

点击configure进行配置，选择第四个选项进行配置，如下图

&nbsp;![2](./2.jpg)

配置工具链

Operating System : ohos

Processor : arm

Compilers c : openharmony源码根目录下的prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang

Compilers c : openharmony源码根目录下的prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang++

Target Root : openharmony源码根目录下的out/hispark_taurus/ipcamera_hispark_taurus/sysroot

如下图所示

&nbsp;![image-20220310101615889](./3.jpg)

点击finish,配置成功后，打开BUILD选项如下图

&nbsp;![4](./4.jpg)

将如下选项中的勾去掉

BUILD_PERF_TESTS

BUILD_TESTS

BUILD_opencv_java_bindings_generator

BUILD_opencv_js_bindings_generator

BUILD_opencv_python_buildings_generator

BUILD_opencv_python_tests



在Search一栏搜索png,把WITH_PNG中的勾去掉，如下图

&nbsp;![5](./5.jpg)



打开CMAKE选项，配置工具链flag

&nbsp;![6](./6.jpg)

CMAKE_CXX_FLAGS : -Wall -fno-exceptions -fPIC -O2 -flto -mfloat-abi=softfp -mfpu=neon-vfpv4 -mfloat-abi=softfp -mfpu=neon-vfpv4 -mcpu=cortex-a7 -fno-common -fno-builtin -fno-strict-aliasing -Wall -mno-unaligned-access -fno-omit-frame-pointer -fstack-protector-all -fexceptions -std=c++14 -fPIC --target=arm-liteos --sysroot=**/root/OpenHarmony_WorkSpace/openharmony-3.1-beta**/out/hispark_taurus/ipcamera_hispark_taurus/sysroot

**注意上述**--sysroot中路径替换成自己目录对应的路径即openharmony源码根目录out/hispark_taurus/ipcamera_hispark_taurus/sysroot



CMAKE_C_FLAGS: -Wall -fno-exceptions -fPIC -O2 -flto -mfloat-abi=softfp -mfpu=neon-vfpv4 -mfloat-abi=softfp -mfpu=neon-vfpv4 -mcpu=cortex-a7 -fno-common -fno-builtin -fno-strict-aliasing -Wall -fsigned-char -mno-unaligned-access -fno-omit-frame-pointer -fstack-protector-all -fPIC --target=arm-liteos --sysroot=**/root/OpenHarmony_WorkSpace/openharmony-3.1-beta**/out/hispark_taurus/ipcamera_hispark_taurus/sysroot

**注意上述**--sysroot中路径替换成自己目录对应的路径即openharmony源码根目录out/hispark_taurus/ipcamera_hispark_taurus/sysroot



配置完成后，点击Configure,Generate,如下图

&nbsp;![7](./7.jpg)



##### 3、修改文件

在opencv源码根目录

```
vim cmake/OpenCVFindLibsGrfmt.cmake

#修改如下代码段
 47   if(NOT JPEG_FOUND)
 48     ocv_clear_vars(JPEG_LIBRARY JPEG_INCLUDE_DIR)
 49
 50    # if(NOT BUILD_JPEG_TURBO_DISABLE)
 51    #   set(JPEG_LIBRARY libjpeg-turbo CACHE INTERNAL "")
 52    #   set(JPEG_LIBRARIES ${JPEG_LIBRARY})
 53    #   add_subdirectory("${OpenCV_SOURCE_DIR}/3rdparty/libjpeg-turbo")
 54    #   set(JPEG_INCLUDE_DIR "${${JPEG_LIBRARY}_SOURCE_DIR}/src" CACHE INTERNAL "")
 55    # else()
 56       set(JPEG_LIBRARY libjpeg CACHE INTERNAL "")
 57       set(JPEG_LIBRARIES ${JPEG_LIBRARY})
 58       add_subdirectory("${OpenCV_SOURCE_DIR}/3rdparty/libjpeg")
 59       set(JPEG_INCLUDE_DIR "${${JPEG_LIBRARY}_SOURCE_DIR}" CACHE INTERNAL "")
 60    # endif()
 61     set(JPEG_INCLUDE_DIRS "${JPEG_INCLUDE_DIR}")
 62   endif()

```

在opencv源码根目录

```
vim include/opencv2/opencv.hpp

#在文件中新增如下头文件
#include "opencv2/core/core_c.h"
#include "opencv2/core/types_c.h"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/highgui/highgui_c.h"
```



##### 4、编译测试

在opencv源码根目录

```
cd build
make -j8
file lib/libopencv_core.so
#打印如下表示已成功编译可以在openharmony L1上运行的动态库
lib/libopencv_core.so: ELF 32-bit LSB shared object, ARM, EABI5 version 1 (SYSV), dynamically linked, not stripped
```



##### 5、将opencv库加入L1编译

在opencv源码根目录新增BUILD.gn

```
vim BUILD.gn

#BUILD.gn中添加如下内容
import("//build/lite/config/component/lite_component.gni")
import("//build/lite/ndk/ndk.gni")

root_build = rebase_path(root_build_dir)

build_ext_component("opencv_lib") {
    command = "sh build_opencv.sh $root_build"
    exec_path = "$root_build/../../../third_party/opencv"
}

lite_component("opencv") {
    deps = [
        ":opencv_lib"
    ]
    features = []
}
```

在opencv源码根目录新增build_opencv.sh

```
touch build_opencv.sh
chmod 777 build_opencv.sh
vim build_opencv.sh

##添加如下内容
#!/bin/sh
processor=`cat /proc/cpuinfo|grep processor | sort -u | wc -l`
cd build
make -j$processor
cp lib/* $1/libs/

```



### 三、移植EasyPR

##### 1、获取源码

```
#将EasyPR库源码放在源码根目录下的third_party下
cd ~/openharmony/third_party
git clone https://github.com/liuruoze/EasyPR.git
```

##### 2、修改文件

在EasyPR源码根目录

重新编写CmakeLists.txt文件

```
rm CMakeLists.txt
vim CMakeLists.txt

#内容如下
cmake_minimum_required(VERSION 3.0.0)
project(easypr)

set(OHOS_SOURCE_ROOT "${PROJECT_SOURCE_DIR}/../..")
set(OHOS_SYSROOT_PATH ${OHOS_SOURCE_ROOT}/out/hispark_taurus/ipcamera_hispark_taurus/sysroot)

set(CMAKE_CROSSCOMPILING TRUE)
set(CMAKE_SYSTEM_NAME ohos)

set(CMAKE_SYSROOT ${OHOS_SYSROOT_PATH})

link_directories("${OHOS_SOURCE_ROOT}/out/hispark_taurus/ipcamera_hispark_taurus/libs")

# where to find header files
include_directories(.)
include_directories(include)
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/build")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/objdetect/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/core/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/world/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/photo/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/videoio/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/imgcodecs/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/ts/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/stitching/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/flann/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/calib3d/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/imgproc/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/features2d/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/dnn/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/ml/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/gapi/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/video/include")
include_directories("${OHOS_SOURCE_ROOT}/third_party/opencv/modules/highgui/include")


# sub directories
add_subdirectory(thirdparty)


# sources to be compiled
set(SOURCE_FILES
        src/core/core_func.cpp
        src/core/chars_identify.cpp
        src/core/chars_recognise.cpp
        src/core/chars_segment.cpp
        src/core/feature.cpp
        src/core/plate_detect.cpp
        src/core/plate_judge.cpp
        src/core/plate_locate.cpp
        src/core/plate_recognize.cpp
        src/core/params.cpp

        src/train/ann_train.cpp
        src/train/annCh_train.cpp
        src/train/svm_train.cpp
        src/train/train.cpp
        src/train/create_data.cpp

        src/util/util.cpp
        src/util/program_options.cpp
        src/util/kv.cpp

        src/interface.cpp
        )

# pack objects to SHARED library
add_library(easypr SHARED  ${SOURCE_FILES})
target_link_libraries(easypr thirdparty)
target_link_libraries(easypr opencv_calib3d)
target_link_libraries(easypr opencv_dnn)
target_link_libraries(easypr opencv_flann)
target_link_libraries(easypr opencv_highgui)
target_link_libraries(easypr opencv_imgproc)
target_link_libraries(easypr opencv_objdetect)
target_link_libraries(easypr opencv_stitching)
target_link_libraries(easypr opencv_core)
target_link_libraries(easypr opencv_features2d)
target_link_libraries(easypr opencv_gapi)
target_link_libraries(easypr opencv_imgcodecs)
target_link_libraries(easypr opencv_ml)
target_link_libraries(easypr opencv_photo)

```



在EasyPR源码根目录

```
cd thirdparty
rm CMakeLists.txt
vim CMakeLists.txt
#写入如下内容
cmake_minimum_required(VERSION 3.1.0)
project(thirdparty)

# sources to be compiled
set(SOURCE_FILES
        xmlParser/xmlParser.cpp
        textDetect/erfilter.cpp
        LBP/helper.cpp
        LBP/lbp.cpp
        mser/mser2.cpp
        )

# pack objects to static library
add_library(thirdparty STATIC ${SOURCE_FILES})
```





在EasyPR源码根目录

```
vim include/easypr/config.h

#如下第4行，修改 CV_VERSION_THREE_ZERO 宏定义为 CV_VERSION_THREE_TWO
1 #ifndef EASYPR_CONFIG_H_
2 #define EASYPR_CONFIG_H_
3
4 #define CV_VERSION_THREE_TWO
5
6 namespace easypr {

```



在EasyPR源码根目录

```
vim include/easypr/util/util.h

#如下第10行，新增__linux__ 宏定义
6 #include <string>
7 #include <vector>
8 #include "opencv2/core/core.hpp"
9
10 #define __linux__
11
12 #if defined(WIN32) || defined(_WIN32)
```



在EasyPR源码根目录

```
vim include/easypr/config.h

#第30至36行，模型文件所在位置，可以自己定义路径，如下
 30 static const char* kDefaultSvmPath = "/sdcard/model/svm_hist.xml";
 31 static const char* kLBPSvmPath = "/sdcard/model/svm_lbp.xml";
 32 static const char* kHistSvmPath = "/sdcard/model/svm_hist.xml";
 33
 34 static const char* kDefaultAnnPath = "/sdcard/model/ann.xml";
 35 static const char* kChineseAnnPath = "/sdcard/model/ann_chinese.xml";
 36 static const char* kGrayAnnPath = "/sdcard/model/annCh.xml";
```



编写车牌识别库对外接口，相关接口使用可以[参考作者文章介绍](https://www.cnblogs.com/subconscious/p/3979988.html)

定义自己需要的接口，下面代码供参考，实现传入一个需要识别的图片的路径，返回一个车牌串，仅限图片中无多车牌的情况

在EasyPR 源码根目录下

```
#添加.h接口
cd include
vim interface.h

#新增如下内容
#ifndef __INTERFACE_H_
#define __INTERFACE_H_
void PlateInit(void);
int GetPlateString(char *path, char *str);
void PlateDeinit(void);
#endif
```



在EasyPR源码根目录下

```
#添加接口实现
cd src
vim interface.cpp

#新增如下内容
#include "easypr.h"
#include <iostream>
#include "interface.h"

using namespace easypr;

CPlateRecognize *pr = NULL;

void PlateInit(void) {
    if (pr == NULL) {
        pr = new CPlateRecognize();
    }

    pr->setResultShow(false);
    pr->setLifemode(true);
    pr->setDetectType(PR_DETECT_COLOR | PR_DETECT_CMSER);
    pr->setMaxPlates(1);
    //这里将模型文件放入sd卡中，也可以自己选择其他目录
    pr->LoadSVM("/sdcard/model/svm_hist.xml");
    pr->LoadANN("/sdcard/model/ann.xml");
    pr->LoadChineseANN("/sdcard/model/ann_chinese.xml");
    pr->LoadGrayChANN("/sdcard/model/annCh.xml");
    pr->LoadChineseMapping("/sdcard/model/province_mapping");
}

int GetPlateString(char *path, char *str) {
    if (pr == NULL) {
        return -1;
    }

    memset(str, 0, strlen(str));

    vector<CPlate> plateVec;
    Mat src = imread(path);
    int row = 720;
    int col = row * src.cols / src.rows;
    resize(src, src, Size(col, row));
    int result = pr->plateRecognize(src, plateVec);

    if (plateVec.size() != 1) {
        return -2;
    }
    strcpy(str, plateVec.at(0).getPlateStr().c_str());
    return 0;
}


void PlateDeinit(void) {
    if (pr != NULL) {
        delete pr;
        pr = NULL;
    }
}
```



##### 3、生成Makefile文件

在EasyPr源码根目录新建build目录

```
mkdir build
cd build
cmake-gui ..
```

&nbsp;![8](./8.jpg)

配置工具链，参考上述opencv移植配置

&nbsp;![3](./3.jpg)

打开CMAKE选项，配置工具链flag，参考上述opencv移植中的配置

&nbsp;![6](./6.jpg)



##### 4、将EasyPR库加入L1编译

在EasyPR源码根目录新增BUILD.gn

```
vim BUILD.gn

#BUILD.gn中添加如下内容
import("//build/lite/config/component/lite_component.gni")
import("//build/lite/ndk/ndk.gni")

root_build = rebase_path(root_build_dir)

build_ext_component("easypr_lib") {
    command = "sh build_easypr.sh $root_build"
    exec_path = "$root_build/../../../third_party/EasyPR"
}

lite_component("easypr") {
    deps = [
        "//third_party/opencv:opencv",
        ":easypr_lib"
    ]
    features = []
}

```

在EasyPR源码根目录新增build_easypr.sh

```
touch build_easypr.sh
chmod 777 build_easypr.sh
vim build_easypr.sh

##添加如下内容
#!/bin/sh
processor=`cat /proc/cpuinfo|grep processor | sort -u | wc -l`
#这里等待opencv库编译完成
while [ ! -e "$1/libs/libopencv_gapi.so" ]
do
    sleep 1
done
cd build
make -j$processor
cp *.so $1/libs/
cp *.so $1
```

将EasyPR源码根目录下的model文件拷贝到sdcard中，若要拷贝到其他目录，同步修改interface.cpp中调用的路径，并将model文件拷贝到对应的路径中



在openharmony源码根目录

```
vim vendor/hisilicon/hispark_taurus/config.json

#将easypr加入到系统中编译，新增如下第23行
17       {
18         "subsystem": "applications",
19         "components": [
20           { "component": "camera_sample_app", "features":[] },
21           { "component": "camera_sample_ai", "features":[] },
22           { "component": "camera_screensaver_app", "features":[] },
23           { "component": "easypr", "features":[] }
24         ]
25       },

```



在openharmony源码根目录

```
vim build/lite/components/applications.json

#新增子组件，如下所示新增193-209行

186         "third_party": [
187           "cjson",
188           "mbedtls",
189           "bounds_checking_function"
190         ]
191       }
192     },
193     {
194       "component": "easypr",
195       "description": "cplate",
196       "optional": "true",
197       "dirs": [
198         "//third_party/EasyPR"
199       ],
200       "targets": [
201         "//third_party/EasyPR:easypr"
202       ],
203       "rom": "",
204       "ram": "",
205       "output": [],
206       "adapted_kernel": [ "liteos_a" ],
207       "features": [],
208       "deps": {}
209     }
210   ]
211 }
```



### 四、编译openharmony固件

```
#这里选择ipcamera_hispark_tauru
hb set
hb build -f
```
