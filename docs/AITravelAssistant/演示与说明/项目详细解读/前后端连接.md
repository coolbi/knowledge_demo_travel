前后端连接
===========
**简要介绍**<br>
>前端采用deveco studio编写，后端部署在华为云服务器上，采用django框架编写。
>景点间的路线距离及耗费时间信息通过高德的api获取。
>后端云服务器开放了端口号1918供前后端连接使用。


# 流程框架
## 后端框架
![后端框架](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/backend_frame.png)
## 文件说明

|文件|说明|
|----|----|
|urls.py|根据前端发送ur跳转至相应的处理函数，进行有关操作。<br>主要指令：'login/'、'register/'、'run/'、'show/'、'detail/'|
|view.py|指令处理，含有5个函数处理5个对应的指令。<br>def login(request)<br>def register(request)<br>def run(request)<br>def show(request)<br>def detail(request)|
|choo.py|根据用户特征及用户需求推荐景点<br>利用已建立好的推荐模型，根据用户的特点计算出用户的特征矩阵。利用用户特征矩阵和景点特征矩阵预测用户喜好，推荐最佳的景点。|
|interest.py|景点详细信息展示<br>根据前端发送的景点名，读取景点表，找到该景点的详细信息后，将数据返回前端|
|arrange.py|路径规划<br>根据用户最终选择的景点，将各个景点全排列后，依据高德地图所给出的距离及花费时间信息，采取最优方案|
|数据文件|checkpoint、save.data-0000-of-0000、save.index、save.meta推荐模型的存储文件<br>user.txt 用户信息存储文件<br>scenary.txt 景点信息存储文件|


# 关键点解读
## 1、url处理
根据前端发送的url跳转至对应函数进行数据的处理。
```python
urlpatterns = [
    path('admin/', admin.site.urls),    
    path('login/',views.login),
    path('register/',views.register),
    path('run/', views.run),
    path('show/',views.show),
    path("detail/",views.detail),
]

```

## 2、指令处理
### a、用户注册
前端<br>
&emsp;用户注册信息传输
```js
////////////点击注册时进行注册信息确认////////////
fetch.fetch({
    url: 'http://122.9.129.166:1918/register/',
    method: "POST",
    data: {
        uid: this.uid,
        pwd: this.pwd,
        gender: this.genderVal,
        age: this.birthdayVal,
        nickname: this.nickname,
        occupation: this.occupationVal,
        region: this.regionVal,
        genre: this.genreVal,
    },
    responseType: "json",
    //返回注册成功/注册失败消息
    success: res => {
        let data = JSON.parse(res.data);
        prompt.showToast({
            message: data["注册信息"],
            duration: 3000
        });
        //注册成功时，跳转到登陆界面
        if (data["注册信息"] == "注册成功") {
            router.push({
                uri: "pages/login/login",
                params: {
                    uid: this.uid,
                    pwd: this.pwd
                } //params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
            });
        }
    },
    //未正常从后端获取消息时展示
    fail: (resp) => {
        prompt.showToast({
            message: "获取数据失败",
            duration: 3000
        });
        //console.log("获取数据失败")
    }
})
```

后端<br>
&emsp;json数据处理
```python
json_data = request.body.decode("utf-8")
mess = json.loads(json_data)             #将json文件转换为字典
data ={
    'uid':mess.get('uid'),
    'pwd':mess.get('pwd'),
    'nickname':mess.get('nickname'),
    'occupation':mess.get('occupation'),
    'age':mess.get('age'),
    'gender':mess.get('gender'),
    'region':mess.get('region'),
    'genre':mess.get('genre')
    }
```
&emsp;文本写入
```python
f = open('D:\\tmp\\goodluck\\goodluck\\luck\\users.txt','a')  
user = str(data['uid']) +':'+str(data['pwd'])+':'\
       +str(data['nickname'])+':'+str(data['occupation'])\
       +':'+str(data['age'])+':'+str(data['gender'])+':'\
       +str(data['region'])+':'+str(data['genre'])+'\n'
suc = f.write(user)   
f.close()
```
### b、用户登录
前端<br>
&emsp;用户登陆数据的传输
```js
//////点击登录按钮，将所输入账号密码返回至后端进行比对///////
login() {
    var info = "";
    fetch.fetch({
        url: 'http://122.9.129.166:1918/login/',
        method: "POST",
        data: {
            uid: this.uid,
            pwd: this.pwd,
        },
        //成功获取信息时
        success: (resp) => {
             info = JSON.parse(resp.data);
             //提示消息：登录成功或信息不正确
             prompt.showToast({
                message: info["i"],
                duration: 3000
            });
            //console.log("data"+info["i"])
            //确认信息无误时将个人喜好信息传给主页面
            if(info["i"] == "登录成功"){
                router.push({
                    uri: "pages/home/home",
                    params: {
                        occupation: info["user"]["occupation"],
                        age: info["user"]["age"],
                        gender: info["user"]["gender"],
                        region: info["user"]["region"],
                        genre: info["user"]["genre"],

                    }//params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
                });
                //console.log(info["user"])
            }
        },
        //信息获取失败时
        fail: (resp) => {
            prompt.showToast({
                message: "获取数据失败",
                duration: 3000
            });
            console.log("获取数据失败")
        }
     });
}
```
后端<br>
&emsp;用户数据表的读取
```python
users_title = ['uid','pwd', 'nickname','occupation','age', 'gender','region','genre']
users = pd.read_table('D:\\tmp\\goodluck\\goodluck\\luck\\users.txt', sep=':', header=None, names=users_title, engine = 'python')
```
&emsp;用户账户密码的比对及用户具体信息的返回
```
try:
    uct = users.loc[users["uid"] == int(data["uid"])].index[0]
    if(str(users.iat[uct,1]) == str(data["pwd"])):
        res["user"]["uid"] = int(users.iat[uct,0])
        res["user"]["pwd"] = str(users.iat[uct,1])
        res["user"]["nickname"] = str(users.iat[uct,2])
        res["user"]["occupation"] = int(users.iat[uct,3])
        res["user"]["age"] = int(users.iat[uct,4])
        res["user"]["gender"] = int(users.iat[uct,5])
        res["user"]["region"] = int(users.iat[uct,6])
        res["user"]["genre"] = int(users.iat[uct,7])
        res["i"] = "登录成功"
    else:
        res["i"] = "输入账号或密码有误"        
except IndexError:
    res["i"] = "账号不存在"
```



### c、景点推荐
前端<br>
&emsp;请求并接收从后端传递的推荐景点信息，并为每个景点创建页面，用于展示景点的具体信息
```js
fetch.fetch({
    url: 'http://122.9.129.166:1918/run/',
    method: "POST",
    data: {
        occupation: this.occupation,
        age: this.age,
        gender: this.gender,
        region: this.region,
        genre: this.genre,
        time : this.time,
        busy : this.busy,
        city : this.city,
    },

    //成功连接并从后端获取到数据时
    success: (resp) => {
        //令获取到的数据赋给info
        info = resp.data;
        //弹窗提示成功信息
        prompt.showToast({
            message: "数据加载中...",
            duration: 3000
        });
        //console.log(info)

        //去引号并且切分各个景点
        info = info.slice(1, -2)
        this.kaka = info.split(',')

        for (var i = 0; i < this.kaka.length; i++) {
            //解码转换成中文
            this.kaka[i] = unescape(this.kaka[i].replace(/\\u/gi,'%u'))
            //分别创建项目对象，可跳转至细节页面
            var item = {
                uri: "pages/detail/detail",
                title: this.kaka[i],
                id: i
            }
            this.index = this.index + 1;
            this.list.push(item); //将新创建的item对象添加到list数组中
        }
        //所建议选择的景点数
        this.numl = this.index - 4;
        this.numn = this.index - 2;
        //转变按钮形态
        this.but = "确定";
    },
    //如果获取数据失败则执行以下函数
    fail: (resp) => {
        prompt.showToast({
            message: "获取数据失败",
            duration: 3000
        });
        //console.log("获取数据失败")
    }
});
```
后端<br>
&emsp;读取已建立模型的所有tensor信息
```python
def get_tensors(self,loaded_graph):
    '''
    从 loaded_graph 中获取tensors
    '''
    user_gender = loaded_graph.get_tensor_by_name("user_gender:0")
    user_age = loaded_graph.get_tensor_by_name("user_age:0")
    user_job = loaded_graph.get_tensor_by_name("user_job:0")
    user_area = loaded_graph.get_tensor_by_name("user_area:0")
    user_ft = loaded_graph.get_tensor_by_name("user_ft:0")
    
    movie_id = loaded_graph.get_tensor_by_name("movie_id:0")
    movie_categories = loaded_graph.get_tensor_by_name("movie_categories:0")
    targets = loaded_graph.get_tensor_by_name("targets:0")
    dropout_keep_prob = loaded_graph.get_tensor_by_name("dropout_keep_prob:0")
    lr = loaded_graph.get_tensor_by_name("LearningRate:0")
    inference = loaded_graph.get_tensor_by_name("inference/ExpandDims:0")
    movie_combine_layer_flat = loaded_graph.get_tensor_by_name("movie_fc/Reshape:0")
    user_combine_layer_flat = loaded_graph.get_tensor_by_name("user_fc/Reshape:0")
    return user_gender, user_age, user_job, user_area, user_ft, movie_id, movie_categories, targets, lr, dropout_keep_prob, inference, movie_combine_layer_flat, user_combine_layer_flat
```
&emsp;载入模型计算出用户特征矩阵
```python
with tf.Session(graph=loaded_graph) as sess:
# 载入保存好的模型
loader = tf.train.import_meta_graph('D:\\tmp\\goodluck\\goodluck\\luck\\save.meta')
loader.restore(sess , 'D:\\tmp\\goodluck\\goodluck\\luck\\save')

# 调用函数拿到 tensors
user_gender, user_age, user_job, user_area, user_ft, movie_id, movie_categories, targets, lr, dropout_keep_prob, _, __,user_combine_layer_flat = self.get_tensors(loaded_graph)  #loaded_graph

# 输入用户基本特征
feed = {
    user_gender: [[gender]],
    user_age: [[age]],
    user_job: [[occupation]],
    user_area: [[region]], 
    user_ft: [[genre,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0,  0, 0,  0]],
    dropout_keep_prob: 1}
# 得到用户特征矩阵
user_combine_layer_flat_val = sess.run([user_combine_layer_flat], feed) 
```
### d、景点详细信息
前端<br>
&emsp;根据从后端获取的景点信息进行展示
```js
//如果获取数据成功的话执行以下函数
success: (resp) => {
    //令获取到的数据赋给info
    info = resp.data;
    //console.log('景点信息'+ info)
    //解码
    info = JSON.parse(info)
    //提示加载中
    prompt.showToast({
        message: "数据加载中...",
        duration: 3000
    });
    this.picture = info["picture"],
    //console.log('图片：'+ this.picture)

    this.jianjie = info["jianjie"]
    //console.log('简介：'+ this.jianjie)
    //切换按钮为返回上一界面
    this.but = "返回上一界面";
},
```

后端<br>
&emsp;根据景点名字寻找景点图片和景点简介

```python
name = eval(name)
ind = scenary.loc[scenary['sname'] == name].index[0]
picture = scenary.iat[ind,1]
jianjie = scenary.iat[ind,2]

```
### e、路径规划
前端<br>
&emsp;将所选择的景点发送给后端，并接收规划好的方案进行展示
```js
success: (resp) => {
    //令获取到的数据赋给info
    info = resp.data;
    info = JSON.parse(info)
    prompt.showToast({
        message: "数据加载中...",
        duration: 3000
    });
    //console.log("info" + info)
    //提取方案信息
    for (var key of Object.keys(info)) {
        //创建方案项目
        var item = {
            index: key,
            route: info[key]["route"],
            duration: info[key]["duration"],
            distance: info[key]["distance"],
            uri: "pages/detail/detail",
        }
        this.list.push(item)
    }
},
```
后端<br>
&emsp;对景点进行排列，通过高德地图得到路线的距离和时间
```python
for idm in itertools.permutations(ids):
    z = ids.index(idm[0])
    y = ids.index(idm[-1])
    ori = pos[z]
    des = pos[y]
    mid = []
    idm = list(idm)
    for i in range(1,len(idm)-1):
        xy = ids.index(idm[i])
        mid.append(pos[xy])
    middle = ';'.join(str(i) for i in mid)
    
    # 连接高德 进行多点路线规划 得到该路线的长度及时间
    url = 'https://restapi.amap.com/v3/direction/driving'
    params = {
        'origin' : ori,
        'destination' : des,
        'waypoints' : middle,
        'extensions' : 'base',
        'strategy' : '10',
        'key' : '0f5f3d3534cb898ff44cfce104bcd59c',
        }
    res = requests.get(url,params)
    
        
    jd = json.loads(res.text)
    a = jd['route']
    b = a['paths']
    # print(b)
    c = b[0]['duration']   
    path.append(idm)
    du.append(int(c))
```


