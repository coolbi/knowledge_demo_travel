## 广播模型

广播用于表明设备在线、传达自身能力、传达自身需求数据，因此需要广播自身数据（名字、类型、id、分组、优先级）、订阅哪些数据、我能发布什么（能输出什么）

### 模型内容

```
{
	messageType: 'broadcast',
	message: {
        "device":{
            "name":"设备名 用于展示方便，开发者查看"
            "type" : "设备类型",
            "id" : "唯一标识符 1. type + '_' + group + '_' + mac经过SHA256加密组成唯一id",  
            "group" : "A_ENTRANCE 分组 A/B/C/D + '_' + ENTRANCE/EXIT 组成", 
            "priority":50 // 优先级 L0 0-99 L1 100-199 L2 200-299 发布时判断是否由自己发布
        }，
        "subscribe" : [], // 需要订阅的消息
        "publish" : ["carApproachDetect","latchControl"] // 发布能力
    }
}
```

### 设备发布能力

参考单播通信模型发布章节 [单播通信模型](UnicastModel.md)

### 感应系统

```
{
	messageType: 'broadcast',
	message: {
        "device":{
            "name":"BES2600超声波检测器"
            "type" : "trigger", 
            "id" : "97234126", 
            "group" : "A_ENTRANCE", 
            "priority":50 // 0-99
        }，
        "subscribe" : [],
        "publish" : ["carApproachDetect","latchControl"]
    }
}
```

### 车牌识别器

```
{
	messageType: 'broadcast',
	message: {
        "device":{
            "name":"3518摄像头'
            "ability" : "camera", 
            "id" : "97234126", 
            "group" : "A_ENTRANCE",
            "priority":150 
        }，
        "subscribe" : [
            "carApproachDetect"
        ],
        "publish" :[ "licensePlateRecognition","latchControl"]
    }
}
```

### 车辆管理中控台

```
{
	messageType: 'broadcast',
	message: {
        "device":{
            "name":"RK3568停车场车辆管理中控台"
            "type" : "parkingSystem", 
            "id" : "97234126", 
            "priority":250，
            "group" : "all",
        }，
        "subscribe" : [
            "carApproachDetect"，"licensePlateRecognition","latchChange"
        ],
        "publish" : ["latchControl"]
    }
}
```

### 门闸控制器

```
{
	messageType: 'broadcast',
	message: {
        "device":{
            "name":"BES2600门闸控制器"
            "type" : "latch", // 设备能力
            "id" : "97234126", // 唯一标识符
            "group" : "A_ENTRANCE", 
            "priority":40  // 0-99
        }，
        "subscribe" : ["latchControl"],
        "publish" : ["latchChange"]
    }
}
```

