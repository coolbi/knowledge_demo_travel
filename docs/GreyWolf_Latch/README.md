# [门闸控制器](../../dev/team_x/GreyWolf_Latch)

## 一、设备展示

### 1. 样例效果

[门闸控制器](../../dev/team_x/GreyWolf_Latch)基于OpenHarmony操作系统，设备控制使用欧智通V200zr开发板，主控芯片为BES2600，系统版本为OpenHarmony3.1 Beta，使用SG90舵机模拟门闸电机。手动按下开启门闸按键时，SG90舵机转动到打开角度、开发板蓝色LED灯点亮。当手动按下关闭门闸按键时，SG90舵机转动到闭合角度、开发板蓝色LED灯熄灭。

![index](media/latch_control.gif)

配合车辆出入检测、车牌识别设备、车辆管理终端设备可对停车场进行车辆出入管理的功能。设备初次上电时，红色LED灯以高频率闪烁，此时设备处于待配网状态。当使用配套手机FA配网后，红色LED灯以较低频率闪烁，此时设备处于连接到路由器状态。当红色LED灯常亮时，代表设备本地组网模块开始工作。

![records](media/latch_net_config.gif)



### 2. 涉及OpenHarmony技术特性

- HDF GPIO

### 3. 支持OpenHarmony版本

- OpenHarmony 3.1 Beta

### 4. 支持开发板

- 欧智通V200ZR开发板套件

## 二、快速上手

#### 1.轻量型设备环境准备

欧智通V200ZR开发套件：

- [BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/bes2600_quick_start#bes2600%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B%E6%8C%87%E5%AF%BC%E6%96%87%E6%A1%A3);

#### 2.项目下载和导入

1）git下载

代码地址：[链接](../../dev/team_x/GreyWolf_Latch)

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_temp.git
```

2）项目导入

```
sudo cp -rfa ~/knowledge_demo_smart_home/dev/team_x/door_access ~/OpenHarmony_3.1_Beta/vendor/team_x
sudo cp -rfa ~/knowledge_demo_smart_home/dev/team_x/common ~/OpenHarmony_3.1_Beta/vendor/team_x
```

#### 3.编译和烧录

参考[BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/bes2600_quick_start#bes2600%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B%E6%8C%87%E5%AF%BC%E6%96%87%E6%A1%A3)完成项目的编译和烧录。

## 三、关键代码解读

#### 1.目录结构

```
.
├─vendor
   └─team_x
       └─door_access
           ├─demo_door_access
           │  ├─external_device //基于HDF的外设驱动
           │  │   ├─key.c // 按键
           |  |   ├─led.c // LED灯
           |  |   └─sg90.c // sg90舵机
           │  ├─local_net // 本地组网通信
           │  │   ├─local_net_communication.c // 本地组网通信协议
           │  │   ├─local_net_udp.c // 本地组网udp通信
           │  │   ├─local_net_dlist.c // 链表存储
           │  │   └─local_net_utils.c // 工具类接口（SHA256等）
           │  ├─iot_main.c// 门闸控制主逻辑
           └─config.json等配置文件及文件夹 设备通用
```

#### 2.关键代码

创建门闸控制任务和LocalNet本地网络通信启动任务。

![](media/iot_main_start_code.png)

门闸控制任务中，按键、LED指示灯、SG90初始化后进入任务循环。循环中读取按键状态。

![](media/latch_control_task_init.png)

根据按键状态，设置门闸开启、关闭、以及路由信息的删除。

![](media/latch_control_task_local_control.png)

当门闸状态发生变化时，且LocalNet本地网络通信模块初始化完成时，发布门闸状态。

![](media/latch_control_task_localnet_send.png)

在本地通信模块启动任务中，首先读取路由信息，当未读取到路由信息时，启动softap配网并设置红色LED灯快速闪烁指示当前状态，否则跳过该步骤。网络配置完成或路由信息读取成功后，连接路由、重新写入路由信息、设置红色LED灯较慢速率闪烁指示当前状态。

![](media/local_net_task_config_and_connect_wifi.png)

配置设备自身信息，并对LocalNet本地通信息模块进行初始化、设置红色LED灯常亮指示当前状态。

![](media/local_net_self_info_set_and_init.png)

## 四、样例联动

本样例由苍狼战队开发，可与其他智能停车场样例组合产品联动效果，参考 [智能停车场](../GreyWolf_SmartParking/README.md)

## 五、参考链接

- [BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/bes2600_quick_start#bes2600%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B%E6%8C%87%E5%AF%BC%E6%96%87%E6%A1%A3)